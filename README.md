Aplikacja jest stworzona dla szerokiego grona muzyków od solistów po zespoły i jej zadaniem jest przerzucenie mnóstwa ogłoszeń z facebook'a, których nikt nie jest w stanie przefiltrować do serwisu ogłoszeń dedykowanego specjalnie do tego.

Docelowo ma być też możliwość tworzenia swojego muzycznego portfolio.
Technologie, które zostały użyte tutaj to:
* Vue.js
* Asp .NET Core 3.1
* Websocket service z paczki SingalR
* Docker spakowany api z bazą i możliwość rozwoju do kolejnych kontenerów

* Nunit do napisania testów controllerów

* Heroku do deploy’u mvp apki(tylko jeszcze z tym się nie wyrobiliśmy)

Z trudniejszych funkcjonalności to napisany chat, który opiera się na komunikacji web socketów, nie wymaga odświeżania strony by widzieć otrzymaną wiadomość od drugiej osoby.

W celu zabezpieczenia i uwierzytelniania stosujemy token JWT, osobiście wole bardziej rozwiązania(token, który jest odświeżany przy każdym request’ście do bazy przez co nie ma opcji ‘podkradnięcia tokenu’), ale na potrzeby projektu została wykorzystana szybsza opcja


 

Dodatkowo dopisane testy sprawdzające poprawność działania controllerów odpowiedzialnych za komunikację z frontem aplikacji

Podsyłam linka do instancji serwera MVP(jest to wersja jeszcze z backend napisany w Ruby on Rails), który widnieje jako testowa - https://mvp-find-band.herokuapp.com/ poglądowo jak to wyglądało


 

W chwili obecnej wygląda to tak https://find-band-staging.herokuapp.com/ jest to etap przejściowy jeszcze z backend’em w Ruby on Rails

Aplikacja ma już przygotowany mockup pod logikę ouath’a z innymi serwisami, ale jeszcze nie wiemy do końca który backend końcowa byśmy chcieli rozwijać