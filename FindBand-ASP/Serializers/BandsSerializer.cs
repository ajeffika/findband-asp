﻿using System.Text.Json;
using System.Text.Json.Serialization;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;
using Newtonsoft.Json.Linq;

namespace FindBand_ASP.Serializers
{
    public class BandsSerializer
    {
        public string Serialize(Band band)
        {
            JObject rss = new JObject(
                new JProperty("data",
                    new JObject(
                        new JProperty("id", band.Id),
                        new JProperty("name", band.Name),
                        new JProperty("description", band.Description)
                    )
                )
            );
            return rss.ToString();
        }
    }
}