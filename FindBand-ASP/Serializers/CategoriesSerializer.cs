﻿using System.Text.Json;
using System.Text.Json.Serialization;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;
using Newtonsoft.Json.Linq;

namespace FindBand_ASP.Serializers
{
    public class CategoriesSerializer
    {
        public string Serialize(Category category)
        {
            JObject rss = new JObject(
                new JProperty("data",
                    new JObject(
                        new JProperty("id", category.Id),
                        new JProperty("name", category.Name),
                        new JProperty("kind", category.Kind)
                    )
                )
            );
            return rss.ToString();
        }
    }
}