﻿using System;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace FindBand_ASP.Entities
{
    public class UsersCategory
    {
        public int Id { get; set; }   
        public virtual User Users { get; set; }
        public int UsersId { get; set; }
        public virtual Category Categories { get; set; }   
        public int CategoriesId { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}