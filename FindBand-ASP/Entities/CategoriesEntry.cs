﻿using System;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class CategoriesEntry
    {
        public int Id { get; set; }   
        public virtual Category Categories { get; set; }
        public int CategoriesId { get; set; }

        public virtual Entry Entries { get; set; }   
        public int EntriesId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}