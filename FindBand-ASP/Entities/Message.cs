﻿using System;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class Message 
    {
        public int Id { get; set; }        
        public virtual User Author { get; set; }   
        public int AuthorId { get; set; }
        public virtual Conversation Conversation { get; set; }    
        public int ConversationId { get; set; }

        public string Content { get; set; }    
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}