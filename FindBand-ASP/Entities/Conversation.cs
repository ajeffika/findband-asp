﻿using System;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class Conversation 
    {
        public int Id { get; set; }        
        public virtual User Inviter { get; set; }
        public int InviterId { get; set; }

        public virtual User Invitee { get; set; }    
        public int InviteeId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}