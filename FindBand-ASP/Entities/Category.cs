﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

namespace  FindBand_ASP.Entities
{
    public class Category
    {
        public int Id { get; set; }   
        public string Name { get; set; }
        public string Kind { get; set; }
        public List<CategoriesEntry> CategoriesEntries { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}