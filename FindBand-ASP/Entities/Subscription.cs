﻿using System;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class Subscription
    {
        public int Id { get; set; }   
        public string Status { get; set; }
        public virtual User Users { get; set; }
        public int UsersId { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}