﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class Entry 
    {
        public int Id { get; set; }        
        public string Title { get; set; }         
        public string Description { get; set; }    
        public string City { get; set; }    
        public string ScopeType { get; set; }    
        public string Phone { get; set; }
        public virtual User Users { get; set; }   
        public List<CategoriesEntry> CategoriesEntries { get; set; }
        public int UsersId { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}