﻿using FindBand_ASP.Entities;
using Microsoft.EntityFrameworkCore;

namespace FindBand_ASP.Models
{
    public class FindBandContext : DbContext
    {
        public FindBandContext(DbContextOptions<FindBandContext> options) : base(options) { }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Band> Bands { get; set; }
        public DbSet<Entry> Entries { get; set; }
        public DbSet<Conversation> Conversations { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<UsersCategory> UsersCategories { get; set; }
        public DbSet<CategoriesEntry> CategoriesEntries { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category() { Id = 1, Name = "pop", Kind = "music" },
                new Category() { Id = 2, Name = "rock", Kind = "music" },
                new Category() { Id = 3, Name = "dance", Kind = "music" },
                new Category() { Id = 4, Name = "electronic", Kind = "music" },
                new Category() { Id = 5, Name = "house", Kind = "music" },
                new Category() { Id = 6, Name = "hip-hop", Kind = "music" },
                new Category() { Id = 7, Name = "classical", Kind = "music" },
                new Category() { Id = 8, Name = "opera", Kind = "music" },
                new Category() { Id = 9, Name = "R&B", Kind = "music" },
                new Category() { Id = 10, Name = "soul", Kind = "music" },
                new Category() { Id = 11, Name = "blues", Kind = "music" },
                new Category() { Id = 12, Name = "metal", Kind = "music" },
                new Category() { Id = 13, Name = "jazz", Kind = "music" });
        }
    }
}