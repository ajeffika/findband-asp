using System.ComponentModel.DataAnnotations;

namespace FindBand_ASP.Models
{
    public class AuthenticateModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}