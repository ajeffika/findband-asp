﻿using System.Collections.Generic;

namespace FindBand_ASP.Models
{
    public class CategoryDto
    {
        public int Id { get; set; }   
        public string Name { get; set; }
        public string Kind { get; set; }
    }
}