﻿using System.Collections.Generic;

namespace FindBand_ASP.Models
{
    public class EntryDto
    {
        public int Id { get; set; }        
        public string Title { get; set; }         
        public string Description { get; set; }    
        public string City { get; set; }    
        public string ScopeType { get; set; }    
        public string Phone { get; set; }
        public int UsersId { get; set; }
        public List<CategoryDto> categories { get; set; }
        public int[] categoryIds { get; set; }

    }
}