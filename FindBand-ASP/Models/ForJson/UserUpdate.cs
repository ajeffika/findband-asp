﻿namespace FindBand_ASP.Models.ForJson
{
    public class UserUpdate
    {
        private string bandId { get; set; }
        private string email { get; set; }
        private string firstName { get; set; }
        private string lastName { get; set; }
        private string username { get; set; }
        private int id { get; set; }
    }
}