﻿using System;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class MessageDto
    {
        public int Id { get; set; }        
        public int authorId { get; set; }
        public int ConversationsId { get; set; }

        public string Content { get; set; }    
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}