﻿using System;

namespace FindBand_ASP.Models
{
    public class CategoryEntryDto
    {
        public int Id { get; set; }   
        public int CategoriesId { get; set; }

        public int EntriesId { get; set; }
    }
}