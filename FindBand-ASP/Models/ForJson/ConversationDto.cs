﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using FindBand_ASP.Models;

namespace  FindBand_ASP.Entities
{
    public class ConversationDto 
    {
        public int Id { get; set; }    
        public string InviterName { get; set; }
        public string InviteeName { get; set; }
        public int InviterId { get; set; }

        public int InviteeId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<CategoryDto> Messages { get; set; }
    }
}