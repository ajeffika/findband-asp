﻿using System;
using FindBand_ASP.Entities;

namespace FindBand_ASP.Models
{
    public class NewMessage 
    {
        public string Content { get; set; }    
        public int AuthorId { get; set; }    
        public int ConversationId { get; set; }    
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}