﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace FindBand_ASP.Models
{
    public class UserModel
    {
        public int Id { get; set; }        
        public string EncryptedPassword { get; set; }    
        public string Username { get; set; }   
        public string Email { get; set; }    
        public string FirstName { get; set; }    
        public string LastName { get; set; }    
        public string Gender { get; set; }    
        public string Role { get; set; }    
        public JsonElement Permissions { get; set; }    
        public string Token { get; set; }    
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}