﻿using System;
using System.Collections.Generic;
using FindBand_ASP.Entities;

namespace FindBand_ASP.Models
{
    public class EntryListElement
    {
        public int Id { get; set; }        
        public string Title { get; set; }         
        public string Description { get; set; }    
        public string City { get; set; }    
        public string ScopeType { get; set; }    
        public string Phone { get; set; }
        public int UsersId { get; set; }
    }
}