﻿using System;

namespace  FindBand_ASP.Entities
{
    public class Bands 
    {
        public int id { get; set; }        
        public string  name { get; set; }         
        public string description { get; set; }    
        
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}