﻿using FindBand_ASP.Entities;
using FindBand_ASP.Models.ForJson;

namespace FindBand_ASP.Models
{
    public class Request
    {
        public NewEntry entry { get; set; } 
        public UserUpdate Object { get; set; } 
        public User auth { get; set; } 
        public Bands band { get; set; } 
        public NewMessage message { get; set; } 
        public ConversationRequest Conversation { get; set; } 
    }
}