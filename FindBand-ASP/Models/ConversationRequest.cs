﻿using System;

namespace FindBand_ASP.Models
{
    public class ConversationRequest
    {
        public int Id { get; set; }        
        public int InviterId { get; set; }

        public int InviteeId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}