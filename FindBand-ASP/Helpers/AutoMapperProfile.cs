using System.Linq;
using AutoMapper;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;

namespace FindBand_ASP.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Message, MessageDto>()
                .ForMember(dto => dto.authorId, opt => opt
                .MapFrom(x => x.Author.Id));
            
            CreateMap<Entry, EntryDto>()
                .ForMember(dto => dto.categoryIds, opt => opt
                    .MapFrom(x => x.CategoriesEntries.Select(a => a.Categories.Id)));
            CreateMap<CategoriesEntry, CategoryEntryDto>()
                .ForMember(res => res.Id, opt => opt
                    .MapFrom(dto => dto.Entries.Id));

            CreateMap<Conversation, ConversationDto>().ForMember(dto => dto.InviterName, opt => opt
                    .MapFrom(x => x.Inviter.FirstName))
                .ForMember(dto => dto.InviteeName, opt => opt
                    .MapFrom(x => x.Invitee.FirstName));
            
            // CreateMap<Entry, EntryDto>();
            // .ForMember(dto => dto.categoriesEntries, opt => opt
            //     .MapFrom(x => x.CategoriesEntries
            //         .Select(y => y.Categories)));
            CreateMap<Category, CategoryDto>();
            CreateMap<Entry, NewEntry>();
            CreateMap<NewEntry, Entry>();
            CreateMap<NewMessage, MessageDto>();
            CreateMap<NewEntry, EntryDto>();
            CreateMap<CategoriesEntry, CategoryEntryDto>();
            CreateMap<User, UserModel>();
            CreateMap<RegisterModel, User>();
        }
    }
}