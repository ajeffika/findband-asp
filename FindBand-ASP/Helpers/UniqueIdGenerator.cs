﻿using System;

namespace FindBand_ASP.Helpers
{
    public class Helper 
    {
         Guid _guid = Guid.NewGuid();

        public string GenerateRandomId()
        {
            var guid = _guid;
            return guid.ToString();
        }
    }
}