﻿using System.Threading.Tasks;
using FindBand_ASP.Entities;
using Microsoft.AspNetCore.Mvc;

namespace FindBand_ASP.Services
{
    public  interface IConversationClient
    {
        Task ConversationCreated(string conversationId);
         Task SendMessage(string message); 
         Task ReceiveMessage(int conversationId); 
    }
}