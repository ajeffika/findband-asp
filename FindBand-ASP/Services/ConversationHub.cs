﻿using System;
using System.Linq;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace FindBand_ASP 
{
    public class ConversationHub : Hub<IConversationClient>
    {
        private readonly FindBandContext _context;
        private readonly IUserService _userService;

        public ConversationHub(FindBandContext context,  IUserService userService)
        {
            _userService = userService; 
            _context = context;
        }

        public async Task ConversationCreated(string userId)
        {
            Conversation conversation;

            var conversations =
                _context.Conversations.Where(i => ((i.InviteeId == Int32.Parse(userId)) && (i.InviteeId == CurrentUser().Id) ));

            if (conversations.Any())
            {
                conversation = _context.Conversations.First(i => (i.InviteeId == Int32.Parse(userId)) && (i.InviteeId == CurrentUser().Id));
            }
            else
            {
                conversation = new Conversation()
                {
                    InviteeId = Int32.Parse(userId),
                    InviterId = CurrentUser().Id,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };
                _context.Add(conversation);
                _context.SaveChanges();
            }
            await Groups.AddToGroupAsync(Context.ConnectionId, conversation.Id.ToString());

        }

        public async Task SendMessage(int conversationId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, conversationId.ToString());
            await Clients.OthersInGroup(conversationId.ToString()).ReceiveMessage(conversationId);
        }

        public async Task ReceiveMessage(int conversationId)
        {
            await Clients.OthersInGroup(conversationId.ToString()).ReceiveMessage(conversationId);
        }
        

        public User CurrentUser()
        {
            return _userService.GetRecordById(Int32.Parse(_userService.GetUserId()));
        }
    }
}