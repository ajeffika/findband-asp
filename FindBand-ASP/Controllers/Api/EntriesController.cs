﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Text.Json.Serialization;
using AutoMapper;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace FindBand_ASP.Controllers.API
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EntriesController : MainController
    {
        private readonly FindBandContext _context;
        private IMapper _mapper;

        public EntriesController(FindBandContext context, IUserService userService, IMapper mapper) : base(userService)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public JsonResult Index(string city, string userId, string scopeType)
        {
            var entries = _context.Entries
                .Include(e => e.CategoriesEntries)
                .ThenInclude(c => c.Categories);
            var entriesFiltered = entries.AsQueryable();;
            if (city != null)
            {
                entriesFiltered = entriesFiltered.Where((i => i.City == city));
            }

            if (userId != null)
            {
                entriesFiltered = entriesFiltered.Where((i => i.UsersId == Int32.Parse(userId)));
            }

            if (scopeType != null)
            {
                entriesFiltered = entriesFiltered.Where((i => i.ScopeType == scopeType));;
            }

            var data = _mapper.Map<IList<Entry>,IList<EntryDto>>(entriesFiltered.ToList()); 

            return Json(_mapper.Map<IList<Entry>,IList<EntryDto>>(entriesFiltered.ToList()));
        }
        [HttpGet]
        [Authorize]
        [Route("{id}")]
        public JsonResult Show(int id)
        {
            var entry = _context.Entries.Include(e => e.CategoriesEntries)
                .ThenInclude(c => c.Categories).First(i => i.Id == id);
            
            return Json(_mapper.Map<Entry, EntryDto>(entry));
        }


        // [HttpGet]
        // [Route("{id}")]
        // public string Show(int id)
        // {
        //     return _serializer.Serialize(_context.Bands.First(i => i.Id == id));
        // }
        //
        [HttpPost]
        [Authorize]
        [Route("")]
        public JsonResult Create([FromBody] Request request)
        {
            var entry = request.entry;
            var entries = new Entry()
            {
                Title = entry.Title,
                Description = entry.Description,
                City = entry.City,
                ScopeType = entry.ScopeType,
                Phone = entry.Phone,
                UsersId = CurrentUser().Id,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            _context.Add(entries);
            _context.SaveChanges();
            foreach (int i in entry.CategoryIds)
            {
                if (_context.CategoriesEntries.Where((c => (c.CategoriesId == i) && (c.EntriesId == entry.Id))).Any())
                {
                    continue;
                }

                var categoryEntry = new CategoriesEntry()
                {
                    CategoriesId = i,
                    EntriesId = entries.Id
                };
                _context.Add(categoryEntry);
                _context.SaveChanges();
            }
            return Json(_mapper.Map<Entry, NewEntry>(entries));
        }
    }
}