﻿using System;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Mvc;

namespace FindBand_ASP.Controllers.API
{
    public abstract class MainController : Controller
    {
        private readonly IUserService _userService;
        
        protected MainController( IUserService userService)
        {
            _userService = userService;
        }
        
        public User CurrentUser()
        {
            return _userService.GetRecordById(Int32.Parse(_userService.GetUserId()));
        }
    }
}