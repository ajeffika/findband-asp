﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace FindBand_ASP.Controllers.API
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CategoriesController : MainController
    {
        private readonly Helper _helper = new Helper();
        private readonly FindBandContext _context;
        private readonly BandsSerializer _serializer = new BandsSerializer(); 

        public CategoriesController(FindBandContext context, IUserService userService) : base(userService)
        {
            _context = context;
        }
        [HttpGet]
        public JsonResult Index(string kind)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };
            var categories = _context.Categories.Where(i => i.Kind == kind);
            return Json(categories);
        }
    }
}