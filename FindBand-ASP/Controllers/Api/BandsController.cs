﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Linq;
using System.Text.Json.Serialization;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers; 
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Newtonsoft.Json.Linq;

namespace FindBand_ASP.Controllers.API
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BandsController : MainController 
    {
        private readonly Helper _helper = new Helper();
        private readonly FindBandContext _context;
        private readonly BandsSerializer _serializer = new BandsSerializer(); 

        public BandsController(FindBandContext context, IUserService userService) : base(userService)
        {
            _context = context;
        }
        
        [HttpGet]
        [Route("{id}")]
        public JsonResult Show(int id)
        {
            return Json(_context.Bands.First(i => i.Id == id));
        }

        [HttpPost]
        [Route("")]
        public JsonResult Create([FromBody] Request request)
        {

            var bands = request.band;
            var band = new Band(){
                Name = bands.name,
                Description = bands.description,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            _context.Add(band);
            _context.SaveChanges();
            return Json(band);
        }
        
        [HttpPost] 
        [Route("{id}")]
        public JsonResult Update(int id, string name, string description)
        {
            var band = (_context.Bands.First(i => i.Id == id));

            band.Name = name;
            band.Description = description;
            _context.SaveChanges();
            return Json(band);
        }
    }
}