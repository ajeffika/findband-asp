using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using AutoMapper;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace FindBand_ASP.Controllers.API
{
    [Authorize]
    [ApiController]
    [Route("api/v1/auth/")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly FindBandContext _context;
        private readonly AppSettings _appSettings;

        public UsersController(FindBandContext context, IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _context = context;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }
        
        [AllowAnonymous]
        [HttpPost("sign-in")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Email, model.Password);

            if (user == null)
                return BadRequest(new { message = "Email or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token
            return Ok(new
            {
                Id = user.Id,
                Username = user.Username,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Token = tokenString,
                Permissions = "",
                BandId = "",
            });
        }

        [AllowAnonymous]
        [HttpPost("sign-up")]
        public IActionResult SignUp([FromBody]RegisterModel registerModel)
        {
            var user = _mapper.Map<User>(registerModel);

            try
            {
                _userService.Create(user, registerModel.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet]
        public string Index()
        {
            return JsonSerializer.Serialize(_context.Users);
        }
        
        // [Authorize]
        // [HttpPatch]    
        // public string Update([FromBody] Request request)
        // {   // [Authorize]
                    // [HttpPut]    
                    // public string Update([FromBody] Request request)
                    // {
                    //     var auth = request.auth;
                    //     var user = _context.Users.First((i) => i.Id == auth.Id);
                    //
                    //     // var user = _context.Users;
                    //     
                    //     var z = request;
                    //     return JsonSerializer.Serialize(_context.Users);
                    // }
        //     // var user = _context.Users
        //     //
    }
}