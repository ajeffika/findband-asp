﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using AutoMapper;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FindBand_ASP.Controllers.API
{
    [Route("api/v1/[controller]")]
    public class ConversationsController : MainController
    {
        private readonly FindBandContext _context;
        private readonly Helper _helper = new Helper();
        private IMapper _mapper;

        public ConversationsController(FindBandContext context,
            IUserService userService, IMapper mapper) : base(userService)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        [Authorize]
        [Route("{conversationId}")]
        public JsonResult Show(int conversationId, int userId)
        {
            var x = CurrentUser().Id;
            Conversation conversation;
            var conversations =
                _context.Conversations.Where(i => (((i.InviteeId == userId) && (i.InviterId == CurrentUser().Id)) ||
                                                   (i.InviteeId == CurrentUser().Id) && (i.InviterId == userId)) ||
                                                  (i.Id == conversationId));
            var z = conversations.Count();
            if (conversations.Any())
            {
                conversation = _context.Conversations.Include(i => i.Invitee)
                    .Include(i => i.Inviter).First(i =>
                    (((i.InviteeId == userId) && (i.InviterId == CurrentUser().Id)) ||
                     (i.InviteeId == CurrentUser().Id) && (i.InviterId == userId)) || (i.Id == conversationId));
            }
            else
            {
                var newConversation = new Conversation()
                {
                    InviteeId = userId,
                    InviterId = CurrentUser().Id,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };
                _context.Add(newConversation);
                _context.SaveChanges();
                conversation = _context.Conversations.Include(i => i.Invitee)
                    .Include(i => i.Inviter).First(i => i.Id == newConversation.Id);
            }

            return Json(conversation);
        }

        [HttpGet]
        [Authorize]
        public JsonResult List(int userId)
        {
            var conversations = _context.Conversations.Include(i => i.Invitee)
                .Include(i => i.Inviter).Where(i =>
                ((i.InviteeId == CurrentUser().Id) || (i.InviterId == CurrentUser().Id))).ToList();

            return Json(_mapper.Map<IList<Conversation>, IList<ConversationDto>>(conversations));
        }
    }
}