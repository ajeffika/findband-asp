﻿using AutoMapper;
using FindBand_ASP.Controllers.API;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Mvc;

namespace FindBand_ASP.Controllers
{
    
    [Route("api/v1/[controller]")]
    [ApiController]

    public class ValidationsController : MainController
    {
        private readonly FindBandContext _context;
        private IMapper _mapper;
        
        public ValidationsController(FindBandContext context, IUserService userService, IMapper mapper) : base(userService)
        {
            _mapper = mapper;
            _context = context;
        }

        [Route("validate-uniqueness")]

        public bool ValidateUniqueness(string valid, string attribute, string model)
        {
            return (true);
        }

    }    
}