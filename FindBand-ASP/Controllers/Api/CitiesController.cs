﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Linq;
using System.Text.Json.Serialization;
using FindBand_ASP.Helpers; 
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Newtonsoft.Json.Linq;

namespace FindBand_ASP.Controllers.API
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CitiesController : MainController 
    {
        private readonly FindBandContext _context;
        private readonly Helper _helper = new Helper();

        public CitiesController(FindBandContext context, IUserService userService) : base(userService)
        {
            _context = context;
        }
        
        [HttpGet]
        [Route("")]
        public JsonResult Index()
        {
            return Json(_context.Entries.Select(i => new { i.City }));
        }
    }
}