﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FindBand_ASP.Controllers.API
{
    [Route("api/v1/[controller]")]
    public class MessagesController : MainController
    {
        private readonly FindBandContext _context;
        private IMapper _mapper;

        public MessagesController(FindBandContext context, IUserService userService, IMapper mapper) : base(userService)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        [Authorize]
        public JsonResult List(int conversationId)
        {
            var messages = _context.Messages.Where(i => (i.ConversationId == conversationId)).ToList();
            return Json(_mapper.Map<IList<Message>,IList<MessageDto>>(messages));
        }
        
        [HttpPost]
        [Authorize]
        [Route("")]

        public JsonResult Create([FromBody] Request request)
        {
            var message = request.message;
            var messages = new Message()
            {
                AuthorId = CurrentUser().Id,
                Content = message.Content,
                ConversationId = message.ConversationId,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            _context.Add(messages);
            _context.SaveChanges();
            return Json(_mapper.Map<MessageDto>(messages));
        }
    }
}