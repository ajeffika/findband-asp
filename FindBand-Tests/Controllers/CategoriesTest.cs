using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using AutoMapper;
using FindBand_ASP.Controllers.API;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Find_Band_Asp_Test
{
    public class CategoriesTest 
    { 
        FindBandDataFixture fixture;
        private CategoriesController _categoriesController;
        private IMapper mapper;


        [SetUp]
        public void Setup()
        {
            var fixtures = new FindBandDataFixture();
            this.fixture = fixtures;
            
            var newHttpContext = CurrentUserHelperTest.SetUpCurrentUser();
            var userService = new UserService(fixture.FindBandContext, newHttpContext);
            this.fixture = fixtures;
            _categoriesController = new CategoriesController(fixture.FindBandContext, userService);
        }

        [Test]
        public void TestIndex()
        {
            JsonResult result = _categoriesController.Index("music");
            var categoriesFiltered = fixture.FindBandContext.Categories.Where(i => i.Kind == "music");
            Assert.AreEqual(new JsonResult(categoriesFiltered).Value,  result.Value );  
        
            Assert.Pass();
        }
    }
}