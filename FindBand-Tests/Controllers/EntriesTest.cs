using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text.Json;
using AspNetCore.RouteAnalyzer.Controllers;
using AutoMapper;
using FindBand_ASP.Controllers.API;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Find_Band_Asp_Test
{
    public class EntriesTest
    {
        FindBandDataFixture fixture;
        private EntriesController entries;
        private IMapper mapper;

        [SetUp]
        public void Setup()
        {
            var newHttpContext = CurrentUserHelperTest.SetUpCurrentUser();
            var fixtures = new FindBandDataFixture();
            this.fixture = fixtures;
            var myProfile = new AutoMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            mapper = new Mapper(configuration);
            var userService = new UserService(fixture.FindBandContext, newHttpContext);

            entries = new EntriesController(fixture.FindBandContext, userService, mapper);
            entries.ControllerContext.HttpContext = new DefaultHttpContext {User = CurrentUserHelperTest.FakeUser()};
        }

        [Test]
        public void TestIndex()
        {
            JsonResult result = entries.Index(null, null, null);
            var entriesFiltered = fixture.FindBandContext.Entries
                .Include(e => e.CategoriesEntries)
                .ThenInclude(c => c.Categories).ToList();
            var resultDeserialized = JsonConvert.SerializeObject(result.Value);
            var assertionJson =
                JsonConvert.SerializeObject(new JsonResult(mapper.Map<IList<Entry>, IList<EntryDto>>(entriesFiltered))
                    .Value);

            Assert.AreEqual(assertionJson, resultDeserialized);

            Assert.Pass();
        }

        [Test]
        public void TestShow()
        {
            JsonResult result = entries.Show(1);
            var entriesFiltered = fixture.FindBandContext.Entries
                .Include(e => e.CategoriesEntries)
                .ThenInclude(c => c.Categories).First(i => i.Id == 1);
            var resultDeserialized = JsonConvert.SerializeObject(result.Value);
            var assertionJson =
                JsonConvert.SerializeObject(new JsonResult(mapper.Map<Entry, EntryDto>(entriesFiltered)).Value);

            Assert.AreEqual(assertionJson, resultDeserialized);

            Assert.Pass();
        }

        [Test]
        public void TestCreate()
        {
            var request = new Request();
            int[] categories = {1, 2};
            var newEntry = new NewEntry()
            {
                Title = "test",
                Description = "testDescription",
                City = "Krakow",
                ScopeType = "music",
                Phone = "123123123",
                UsersId = 1,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                CategoryIds = categories,
            };
            request.entry = newEntry;
            JsonResult result = entries.Create(request);
            request.entry.Id = 2;
            var resultDeserialized = result.Value as NewEntry;
            var assertionJson = new JsonResult((newEntry)).Value as NewEntry;

            Assert.AreEqual(assertionJson.Description, resultDeserialized.Description);

            Assert.Pass();
        }
    }
}