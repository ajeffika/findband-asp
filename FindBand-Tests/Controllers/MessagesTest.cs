﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text.Json;
using AspNetCore.RouteAnalyzer.Controllers;
using AutoMapper;
using FindBand_ASP.Controllers.API;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Find_Band_Asp_Test
{
    public class MessagesTest
    {
        FindBandDataFixture fixture;
        private MessagesController _messagesController;
        private IMapper mapper;

        [SetUp]
        public void Setup()
        {
            var newHttpContext = CurrentUserHelperTest.SetUpCurrentUser();
            var fixtures = new FindBandDataFixture();
            this.fixture = fixtures;
            var myProfile = new AutoMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            mapper = new Mapper(configuration);
            var userService = new UserService(fixture.FindBandContext, newHttpContext);

            _messagesController = new MessagesController(fixture.FindBandContext, userService, mapper);
            _messagesController.ControllerContext.HttpContext = new DefaultHttpContext
                {User = CurrentUserHelperTest.FakeUser()};
        }

        [Test]
        public void TestList()
        {
            JsonResult result = _messagesController.List(1);
            var messages = fixture.FindBandContext.Messages.Where(i => i.ConversationId == 1).ToList();
            var resultDeserialized = JsonConvert.SerializeObject(result.Value);
            var assertionJson =
                JsonConvert.SerializeObject(new JsonResult(mapper.Map<IList<Message>, IList<MessageDto>>(messages))
                    .Value);

            Assert.AreEqual(assertionJson, resultDeserialized);

            Assert.Pass();
        }


        [Test]
        public void TestCreate()
        {
            var request = new Request();
            var newMessage = new NewMessage()
            {
                AuthorId = 1,
                Content = "test",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                ConversationId = 1,
            };
            request.message = newMessage;
            JsonResult result = _messagesController.Create(request);
            var resultDeserialized = result.Value as MessageDto;
            var assertionJson = new JsonResult((newMessage)).Value as NewMessage;

            Assert.AreEqual(mapper.Map<NewMessage, MessageDto>(assertionJson).Content, resultDeserialized.Content);

            Assert.Pass();
        }
    }
}