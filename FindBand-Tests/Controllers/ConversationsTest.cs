﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text.Json;
using AspNetCore.RouteAnalyzer.Controllers;
using AutoMapper;
using FindBand_ASP.Controllers.API;
using FindBand_ASP.Entities;
using FindBand_ASP.Helpers;
using FindBand_ASP.Models;
using FindBand_ASP.Serializers;
using FindBand_ASP.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Find_Band_Asp_Test
{
    public class ConversationsTest
    {
        FindBandDataFixture fixture;
        private ConversationsController _conversationsController;
        private IMapper mapper;

        [SetUp]
        public void Setup()
        {
            var fixtures = new FindBandDataFixture();
            this.fixture = fixtures;
            var newHttpContext = CurrentUserHelperTest.SetUpCurrentUser();
            var myProfile = new AutoMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            mapper = new Mapper(configuration);
            var userService = new UserService(fixture.FindBandContext, newHttpContext);
            
            _conversationsController = new ConversationsController(fixture.FindBandContext, userService, mapper);
            _conversationsController.ControllerContext.HttpContext = new DefaultHttpContext
                {User = CurrentUserHelperTest.FakeUser()};
        }

        [Test]
        public void TestList()
        {
            var currentUserId = 1;

            var conversations = fixture.FindBandContext.Conversations.Include(i => i.Invitee)
                .Include(i => i.Inviter).Where(i =>
                    ((i.InviteeId == currentUserId) || (i.InviterId == currentUserId))).ToList();
            
  
            JsonResult result = _conversationsController.List(currentUserId);

            var resultDeserialized = result.Value as IList<ConversationDto>;
            var assertionJson = new JsonResult((conversations)).Value as IList<Conversation>;

            Assert.AreEqual(mapper.Map<IList<Conversation>, IList<ConversationDto>>(assertionJson).Count(), resultDeserialized.Count());

            Assert.Pass();
        }


        [Test]
        public void TestShowCreate()
        {
            var conversationId = 0;
            var userId = 2;
            var currentUserId = 1;
            
            var conversation =
                fixture.FindBandContext.Conversations.First(i => (((i.InviteeId == userId) && (i.InviterId == currentUserId)) ||
                                                                  (i.InviteeId == currentUserId) && (i.InviterId == userId)) ||
                                                                 (i.Id == conversationId));
  
            JsonResult result = _conversationsController.Show(conversationId, userId);

            var resultDeserialized = result.Value as ConversationDto;
            var assertionJson = new JsonResult((conversation)).Value as ConversationDto ;

            Assert.AreEqual(assertionJson, resultDeserialized);

            Assert.Pass();
        }
    }
}