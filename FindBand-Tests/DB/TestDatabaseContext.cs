﻿using System;
using FindBand_ASP.Entities;
using FindBand_ASP.Models;
using Microsoft.EntityFrameworkCore;

public class FindBandDataFixture : IDisposable
{   
    public FindBandContext FindBandContext { get; private set; }
   

    public FindBandDataFixture()
    {
        string myDatabaseName = "find-band-asp-"+DateTime.Now.ToFileTimeUtc();

        var options = new DbContextOptionsBuilder<FindBandContext>()
            .UseInMemoryDatabase(databaseName: myDatabaseName )
            .Options;
         FindBandContext  = new FindBandContext(options);

         FindBandContext.Categories.Add(new Category() { Id = 1, Name = "test1", Kind = "music" });
         FindBandContext.Categories.Add(new Category() { Id = 2, Name = "test2", Kind = "music" });
         FindBandContext.Users.Add(new User() { Id = 1, FirstName = "test1", Username = "xxx", Email = "test@test.com"});
         FindBandContext.Users.Add(new User() { Id = 2, FirstName = "test2", Username = "xxx2", Email = "test2@test2.com"});
         FindBandContext.Users.Add(new User() { Id = 3, FirstName = "test3", Username = "xxx3", Email = "test3@test3.com"});
         FindBandContext.Entries.Add(new Entry() { Id = 1, Title = "test1", UsersId = 1, Description = "Entry lel"});
         FindBandContext.CategoriesEntries.Add(new CategoriesEntry() { Id = 1, CategoriesId = 1, EntriesId = 1});
         FindBandContext.Messages.Add(new Message() { Id = 1, AuthorId = 1, Content = "Test", ConversationId = 1});
         FindBandContext.Conversations.Add(new Conversation() { Id = 1, InviteeId = 1, InviterId = 2});
         FindBandContext.Conversations.Add(new Conversation() { Id = 2, InviteeId = 3, InviterId = 2});
         FindBandContext.SaveChanges();
    }

    public void Dispose()
    {
        FindBandContext.Dispose();
    }
}