﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace Find_Band_Asp_Test
{
    public class CurrentUserHelperTest
    {
        public static ClaimsPrincipal FakeUser()
        {
            return new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "SomeValueHere"),
                new Claim(ClaimTypes.Name, "1")
            }, "TestAuthentication"));
        }

        public static HttpContextAccessor SetUpCurrentUser()
        {
            var newHttpContext = new HttpContextAccessor();
            newHttpContext.HttpContext = new DefaultHttpContext {User = FakeUser()};
            return newHttpContext;
        }
    }
}